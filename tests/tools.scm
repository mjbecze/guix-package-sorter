(define-module (test-import-tools)
  #:use-module (srfi srfi-64)
  #:use-module (guix tests)
  #:use-module (guix scripts sort)
  #:use-module (guix scripts merge))

(define unsorted
  ";; some comments on top that shouldn't get sorted
(define-module (foo bar)
  #:use-module (baz))
(define-public za #t)
(define-public ccc #t)
(define-public yyy #t)
(define-public aaa #t)
(define-public dd #t)
;; this is a comment for zz
(define-public zz #t)
(define-public ca #t)")

(define expected-sort-result
  ";; some comments on top that shouldn't get sorted
(define-module (foo bar)
  #:use-module (baz))
(define-public aaa #t)
(define-public ca #t)
(define-public ccc #t)
(define-public dd #t)
(define-public yyy #t)
(define-public za #t)
;; this is a comment for zz
(define-public zz #t)")

(define unsorted-with-inhert
 "(define-module rust-criterion-0.3) 
(define-module rust-criterion-0.1) 
(define-module rust-criterion-0.2
  package
    (inherit rust-criterion-0.3))")

(define expected-with-inhert
  "(define-module rust-criterion-0.1)
(define-module rust-criterion-0.3)  
(define-module rust-criterion-0.2
  package
    (inherit rust-criterion-0.3))")

(test-begin "sort")
(test-equal "should basic sort"
  expected-sort-result
  (mock ((ice-9 ports) open-file
       (lambda (filename mode)
	 (open-input-string unsorted)))
      (call-with-output-string
	(lambda (port)
	  (with-output-to-port port
	    (lambda ()
	      (guix-sort "./tests/tools-unsorted-data.scm")))))))

(test-equal "should sort with respect to the inherit clause"
  expected-sort-result
  (mock ((ice-9 ports) open-file
       (lambda (filename mode)
	 (open-input-string unsorted)))
      (call-with-output-string
	(lambda (port)
	  (with-output-to-port port
	    (lambda ()
	      (guix-sort "./tests/tools-unsorted-data.scm")))))))

(test-end "sort")
