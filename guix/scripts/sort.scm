(define-module (guix scripts sort)
  #:use-module (guix i18n)
  #:use-module (ice-9 match)
  #:use-module (ice-9 binary-ports)
  #:export (guix-sort))

(define* (package-list port #:optional (init '()))
  (let ((start (ftell port))
	(sexp (read port))
	(end (ftell port)))
    (if (eof-object? sexp)
	init
	(package-list port (cons (list start end sexp)  init)))))


(define package-name-string
  (match-lambda 
    [('define-public name rest)
     (string-append (package-name-string rest)
		    (symbol->string name))]
    [(_ *** ('inherit name)) (symbol->string name)]
    [x ""]))

(define (sort-packages packages)
  (sort-list
   packages
   (match-lambda* (((_ _ packages) ...)
		   (apply string<? (map package-name-string packages))
		   )
		  (_ #t))))

(define (write-packages packages in-port out-port)
  (for-each (match-lambda
	      ((start end _)
	       (let* ((len (- end start)))
		 (seek in-port start SEEK_SET)
		 (put-bytevector
		  out-port
		  (get-bytevector-n in-port len)))))
	    packages))

(define (show-help)
  (display "help"))

(define (guix-sort . args)
  (match args
    (()
     (format (current-error-port)
	     (G_ "Please give me a file to sort")))
    ((or ("-h") ("--help"))
     (show-help)
     (exit 0))
    ((filename)
     (let* ((out-port (current-output-port))
	    (in-port  (open-file filename "r"))
	    (packages (package-list in-port))
	    (sorted-packages (sort-packages packages)))
       (write-packages sorted-packages in-port out-port)))))
